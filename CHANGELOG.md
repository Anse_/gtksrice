# Changelog
All notable changes to this project will be documented in this file.

## V1.1 [Minor UPDATE] (07/18/2020)
- Fix an issue that makes the app crash when clicking on the "Check Process" button.
- Disable the LaTeX render option because I still don't have the authorization to use the service.

## V1.0  [INITIAL RELEASE] (07/14/2020)
- Add the first files such as all the source code plus the documentation in French (the English one will come soon).
