﻿using System;
using System.Collections.Generic; // In order to create lists
using System.IO;
using System.Linq; // In order to compaire elements (Min for xmax)
using System.Net;
using Gdk;
using Gtk;

public partial class MainWindow : Gtk.Window
{

    // First lists which contain all informations needed to the computation.
    List<string> name_input = new List<string> { };
    List<decimal> coef_input = new List<decimal> { };
    List<string> n_input = new List<string> { };

    List<string> name_output = new List<string> { };
    List<decimal> coef_output = new List<decimal> { };
    List<string> n_output = new List<string> { };

    List<decimal> reagent_calculus = new List<decimal> { };
    List<decimal> product_calculus = new List<decimal> { };

    // Reagent / Product switches.
    bool name_bool = false;
    bool coef_bool = false;
    bool n_bool = false;

    string equation_formula_text = string.Empty;
    decimal xmax;
    int i;

    public MainWindow() : base(Gtk.WindowType.Toplevel) => Build();

    protected void OnDeleteEvent(object sender, DeleteEventArgs a)
    {
        Application.Quit();
        a.RetVal = true;
    }


    // all created voids are here

    void Add_name()
    {
        if (entry_name.Text == "/")
        {
            name_bool = name_bool == false ? true : false;
        }
        else if (!(entry_name.Text == "" | entry_name.Text == " "))
        {
            if (name_bool == false)
            {
                name_input.Add(entry_name.Text);
            }
            else name_output.Add(entry_name.Text);
        }
        else
        {
            MessageDialog md = new MessageDialog(this, DialogFlags.NoSeparator, MessageType.Error, ButtonsType.Ok, "You can't add nothing !");
            md.Run();
            md.Destroy();
        }

        entry_name.Text = string.Empty;
        Refresh();
    }
    void Delete_name()
    {
        name_input.Clear();
        name_output.Clear();
        entry_name.Text = string.Empty;
        name_bool = false;
        Refresh();
    }

    void Add_coef()
    {
        if (entry_coef.Text == "/")
        {
            coef_bool = coef_bool == false ? true : false;
        }
        else if (decimal.TryParse(entry_coef.Text, out decimal decimalcoef))
        {
            if (coef_bool == false)
            {
                coef_input.Add(decimalcoef);
            }
            else coef_output.Add(decimalcoef);
        }
        else
        {
            MessageDialog md = new MessageDialog(this, DialogFlags.NoSeparator, MessageType.Error, ButtonsType.Ok, "Error : You must add Reals!");
            md.Run();
            md.Destroy();
        }

        entry_coef.Text = string.Empty;
        Refresh();
    }
    void Delete_coef()
    {
        coef_input.Clear();
        coef_output.Clear();
        entry_coef.Text = string.Empty;
        coef_bool = false;
        Refresh();
    }

    void Add_quantity()
    {
        entry_quantity.Text = entry_quantity.Text.Replace(".", ",");
        if (entry_quantity.Text == "/")
        {
            n_bool = n_bool == false ? true : false;
        }
        else if (entry_quantity.Text == string.Empty ^ entry_quantity.Text == " ")
        {
            MessageDialog md = new MessageDialog(this, DialogFlags.NoSeparator, MessageType.Error, ButtonsType.Ok, "You can't add nothing !");
            md.Run();
            md.Destroy();
            return;
        }
        else if (entry_quantity.Text == "*")
        {
            if (n_bool == false)
            {
                n_input.Add(entry_quantity.Text);
            }
            else
                n_output.Add(entry_quantity.Text);
        }
        else if (decimal.TryParse(entry_quantity.Text, out decimal decimalquantity))
        {
            if (n_bool == false)
            {
                n_input.Add(entry_quantity.Text);
            }
            else n_output.Add(entry_quantity.Text);
        }
        else
        {
            MessageDialog md = new MessageDialog(this, DialogFlags.NoSeparator, MessageType.Error, ButtonsType.Ok, "You can add Reels !");
            md.Run();
            md.Destroy();
        }

        entry_quantity.Text = string.Empty;
        Refresh();
    }
    void Delete_quantity()
    {
        n_input.Clear();
        n_output.Clear();
        entry_quantity.Text = string.Empty;
        n_bool = false;
        Refresh();
    }


    void Check_process()
    {
        if (name_input.Count != coef_input.Count ^ name_input.Count != n_input.Count ^ name_input.Count != name_output.Count ^ name_input.Count != coef_output.Count ^ name_input.Count != n_output.Count)
        {
            MessageDialog md = new MessageDialog(this, DialogFlags.NoSeparator, MessageType.Error, ButtonsType.Ok, "Process cannot start : every entry doesn't have the same amount, please check if you didn't one(s) somewhere !");
            md.Run();
            md.Destroy();
        }

        else if (name_input.Count < 2)
        {
            MessageDialog md = new MessageDialog(this, DialogFlags.NoSeparator, MessageType.Error, ButtonsType.Ok, "Process cannot start : You need at least 2 reagents !");
            md.Run();
            md.Destroy();
        }
        else
        {
            Refresh();
        }

    }
    void Refresh()
    {
        if (name_input.Count != 0 | name_output.Count != 0)
        {
            string equation_formula_input = "Name of reagents: ";
            for (i = 0; i < name_input.Count; i++)
            {

                equation_formula_input = equation_formula_input + name_input[i];
                if (i < name_input.Count - 1)
                {
                    equation_formula_input = equation_formula_input + " ; ";
                }
            }

            string equation_formula_output = "Name of product(s): ";
            for (i = 0; i < name_output.Count; i++)
            {

                equation_formula_output = equation_formula_output + name_output[i];
                if (i < name_output.Count - 1)
                {
                    equation_formula_output = equation_formula_output + " ; ";
                }
            }
            string equation_coef_input = "Coefficients: ";
            for (i = 0; i < coef_input.Count; i++)
            {

                equation_coef_input = equation_coef_input + coef_input[i];
                if (i < coef_input.Count - 1)
                {
                    equation_coef_input = equation_coef_input + " ; ";
                }
            }

            string equation_coef_output = "Coefficient(s): ";
            for (i = 0; i < coef_output.Count; i++)
            {

                equation_coef_output = equation_coef_output + coef_output[i];
                if (i < coef_output.Count - 1)
                {
                    equation_coef_output = equation_coef_output + " ; ";
                }
            }

            string equation_n_input = "Ammount: ";
            for (i = 0; i < n_input.Count; i++)
            {
                equation_n_input = n_input[i] != "*" ? equation_n_input + n_input[i] + " mol" : equation_n_input + "excess";
                if (i < n_input.Count - 1)
                {
                    equation_n_input = equation_n_input + " ; ";
                }
            }

            string equation_n_output = "Ammount: ";
            for (i = 0; i < n_output.Count; i++)
            {
                equation_n_output = n_output[i] != "*" ? equation_n_output + n_output[i] + " mol" : equation_n_output + "excess";

                if (i < n_output.Count - 1)
                {
                    equation_n_output = equation_n_output + " ; ";
                }
            }
            textview1.Buffer.Text = equation_formula_text + Environment.NewLine + equation_formula_input + Environment.NewLine + equation_coef_input + Environment.NewLine + equation_n_input + Environment.NewLine + Environment.NewLine;
            textview1.Buffer.Text = textview1.Buffer.Text + equation_formula_output + Environment.NewLine + equation_coef_output + Environment.NewLine + equation_n_output + Environment.NewLine;
        }
    }
    void Latexion()
    {
        equation_formula_text = string.Empty;
        for (i = 0; i < coef_input.Count; i++)
        {
            equation_formula_text = equation_formula_text + coef_input[i].ToString() + " " + name_input[i];
            if (i < name_input.Count - 1)
            {
                equation_formula_text = equation_formula_text + " + ";
            }
            else
            {
                equation_formula_text = equation_formula_text + "\\longrightarrow";
            }
        }

        for (i = 0; i < coef_output.Count; i++)
        {
            equation_formula_text = equation_formula_text + coef_output[i].ToString() + " " + name_output[i];
            if (i < name_output.Count - 1)
            {
                equation_formula_text = equation_formula_text + " + ";
            }
            else { }
        }

        if (check_imagevisible.Active == true)
        {
            string url = "http://www.texrendr.com/cgi-bin/mimetex?\\small " + equation_formula_text;
            Uri uri = new Uri(url);
            WebClient client = new WebClient();
            client.DownloadFile(uri, Directory.GetCurrentDirectory() + "/LaTeX.gif");
            image1.Pixbuf = new Pixbuf(Directory.GetCurrentDirectory() + "/LaTeX.gif");
        }
    }


    void Process_xmaxfinder()
    {
        try
        {
            textview1.Buffer.Text = textview1.Buffer.Text + "_____" + Environment.NewLine + "xmax finder:" + Environment.NewLine;

            List<decimal> xf = new List<decimal> { };

            for (i = 0; i < name_input.Count; i++)
            {
                if (n_input[i] == "*")
                {
                    textview1.Buffer.Text = textview1.Buffer.Text + name_input[i] + " is surplus" + Environment.NewLine;
                }
                else
                {
                    xf.Add(decimal.Parse(n_input[i]) / coef_input[i]);
                    textview1.Buffer.Text = textview1.Buffer.Text + "xf for " + name_input[i] + " = " + xf.Last() + Environment.NewLine;
                }
            }

            xmax = xf.Min();
            textview1.Buffer.Text = textview1.Buffer.Text + "xmax = " + xmax.ToString();
        }
        catch (Exception xmaxfinder)
        {
            File.WriteAllText(xmaxfinder.ToString(), Directory.GetCurrentDirectory() + "xmaxfinder error.txt");
        }
    }
    void Process_computation(decimal x)
    {
        try
        {
            reagent_calculus.Clear();
            product_calculus.Clear();

            textview1.Buffer.Text = textview1.Buffer.Text + Environment.NewLine + "_____" + Environment.NewLine + "for " + x + " mol" + ":" + Environment.NewLine;
            for (i = 0; i < name_input.Count; i++)
            {
                if (n_input[i] == "*")
                {
                    textview1.Buffer.Text = textview1.Buffer.Text + name_input[i] + " is surplus" + Environment.NewLine;
                }
                else
                {
                    reagent_calculus.Add(decimal.Parse(n_input[i]) - (x * coef_input[i]));
                    textview1.Buffer.Text = textview1.Buffer.Text + name_input[i] + ": " + reagent_calculus[i] + " mol | With : decimal.Parse(n_input[i]) " + decimal.Parse(n_input[i]) + " - (x * coef_input[i]) " + (x * coef_input[i]) + Environment.NewLine;
                }
            }
            textview1.Buffer.Text = textview1.Buffer.Text + Environment.NewLine;
            for (i = 0; i < name_output.Count; i++)
            {
                if (n_output[i] == "*")
                {
                    textview1.Buffer.Text = textview1.Buffer.Text + name_output[i] + " is surplus";
                }
                else
                {
                    product_calculus.Add(decimal.Parse(n_output[i]) + x * coef_output[i]);
                    textview1.Buffer.Text = textview1.Buffer.Text + name_output[i] + ": " + product_calculus[i] + " mol" + Environment.NewLine;
                }
            }
            textview1.Buffer.Text = textview1.Buffer.Text + Environment.NewLine;
        }
        catch (Exception computation)
        {
            File.WriteAllText(computation.ToString(), Directory.GetCurrentDirectory() + "computation error.txt");
        }
    }
    void Stoichiometric()
    {
        decimal sum = 0;
        sum = reagent_calculus.Sum();

        textview1.Buffer.Text = textview1.Buffer.Text + Environment.NewLine + "_____" + Environment.NewLine + "Is this reaction stoichiometric?" + Environment.NewLine + "Σ = " + sum + Environment.NewLine;

        textview1.Buffer.Text = sum == 0
            ? textview1.Buffer.Text + "This reaction is stoichiometric."
            : textview1.Buffer.Text + "This reaction is not stoichiometric.";

    }

    #region Generated Events

    protected void OnEntryNameActivated(object sender, EventArgs e)
    {

        Add_name();

    }

    protected void OnAddNameClicked(object sender, EventArgs e)
    {
        Add_name();

    }

    protected void OnDeleteNameClicked(object sender, EventArgs e)
    {
        Delete_name();
    }

    protected void OnEntryCoefActivated(object sender, EventArgs e)
    {
        Add_coef();
    }

    protected void OnAddCoefClicked(object sender, EventArgs e)
    {
        Add_coef();
    }

    protected void OnDeleteCoefClicked(object sender, EventArgs e)
    {
        Delete_coef();
    }

    protected void OnEntryQuantityActivated(object sender, EventArgs e)
    {
        Add_quantity();

    }

    protected void OnAddQuantityClicked(object sender, EventArgs e)
    {
        Add_quantity();
    }

    protected void OnDeleteQuantityClicked(object sender, EventArgs e)
    {
        Delete_quantity();
    }

    protected void OnButtonDelallClicked(object sender, EventArgs e)
    {
        check_imagevisible.Active = false;
        Delete_name();
        Delete_coef();
        Delete_quantity();
    }

    protected void OnButtonCheckClicked(object sender, EventArgs e)
    {
        Check_process();
        Latexion();
    }

    protected void OnButtonProcessClicked(object sender, EventArgs e)
    {
        Refresh();
        Latexion();
        Process_xmaxfinder();
        Process_computation(0);
        Process_computation(xmax);
        Stoichiometric();
    }

    protected void OnButtonExample1Clicked(object sender, EventArgs e)
    {
        Delete_name();
        Delete_coef();
        Delete_quantity();

        name_input.Add("MnO_4^-");
        name_input.Add("Fe^{2+}");
        name_input.Add("H^+");

        coef_input.Add(1);
        coef_input.Add(5);
        coef_input.Add(8);

        n_input.Add("0,1");
        n_input.Add("0,5");
        n_input.Add("*");


        name_output.Add("Mn^{2+}");
        name_output.Add("Fe^{3+}");
        name_output.Add("H_2O");

        coef_output.Add(1);
        coef_output.Add(5);
        coef_output.Add(4);

        n_output.Add("0");
        n_output.Add("0");
        n_output.Add("*");

        MessageDialog md = new MessageDialog(this, DialogFlags.Modal, MessageType.Info, ButtonsType.Ok, "Data replaced successfully!");
        md.Run();
        md.Destroy();

        Latexion();
        Refresh();

    }

    #endregion
}

